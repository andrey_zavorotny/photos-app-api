from typing import Optional, List
from uuid import uuid4

from app.db.errors import EntityDoesNotExist
from app.db.queries.queries import queries
from app.db.repositories.base import BaseRepository
from app.models.photos import Photo, PhotoInDB, PhotoList, PhotoListWithPositions, PositionedPhoto


class PhotosRepository(BaseRepository):
    async def get_photos_by_user_id(self, user_id: int) -> PhotoList:
        photos = await queries.get_photos_by_user_id(
            self.connection,
            user_id=user_id
        )
        if photos:
            photos_list = [Photo(**p)for p in photos]
            return PhotoList(photos=photos_list)
        else:
            return PhotoList()

    async def get_unnatached_photos_by_user_id(self, user_id: int) -> PhotoList:
        photos = await queries.get_unattached_photos_by_user_id(
            self.connection,
            user_id=user_id
        )
        if photos:
            photos_list = [Photo(**p)for p in photos]
            return PhotoList(photos=photos_list)
        else:
            return PhotoList()

    async def get_photos_by_gallery_id(self, gallery_id: int, user_id: int) -> PhotoListWithPositions:
        photos = await queries.get_photos_by_gallery_id(
            self.connection,
            gallery_id=gallery_id,
            user_id=user_id
        )
        if photos:
            photos_list = [PositionedPhoto(**p)for p in photos]
            return PhotoListWithPositions(photos=photos_list)
        else:
            return PhotoListWithPositions()

    async def get_photo_by_photo_uuid(self, photo_uuid: str, user_id: int) -> PhotoInDB:
        photo = await queries.get_photos_by_gallery_id(
            self.connection,
            photo_uuid=photo_uuid,
            user_id=user_id
        )
        if photo:
            return PhotoInDB(**photo)
        else:
            raise EntityDoesNotExist

    async def add_photos_by_user_id(
        self,
        user_id: int,
        photos: List[PhotoInDB],
        gallery_id: Optional[int]
    ) -> PhotoList:
        if not gallery_id:
            gallery_id = await queries.get_unattached_gallery_id(self.connection, user_id)

        photos_batch_data, attachments_batch_data = [], []
        for photo in photos:
            photo_uuid = str(uuid4)  # TODO async uuid4 implementation
            photos_batch_data.append({
                "photo_uuid": photo_uuid,
                "metadata": photo.metadata
            })
            attachments_batch_data.append({
                "photo_uuid": photo_uuid,
                "gallery_id": gallery_id
            })

        async with self.connection.transaction():
            await queries.add_photos(
                self.connection,
                photos_batch_data
            )
            await queries.attach_photos_to_gallery(
                self.connection,
                attachments_batch_data
            )

        photos_list = [Photo(photo_uuid=p.get("photo_uuid"))
                       for p in photos_batch_data]
        return PhotoList(photos=photos_list)
