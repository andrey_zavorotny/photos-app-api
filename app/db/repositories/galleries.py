from decimal import Decimal
from typing import Dict, List, Optional, Tuple
from uuid import UUID

from app.db.errors import EntityDoesNotExist
from app.db.queries.queries import queries
from app.db.repositories.base import BaseRepository
from app.models.galleries import (GalleryInDB, GalleryList,
                                  GalleryListWithPositions, GalleryWithId,
                                  PositionedGallery)
from app.models.photos import Photo, PhotoList, PhotoListWithPositions
from app.models.shared_link import SharedLink
from app.utils.positions import find_positions


class GalleriesRepository(BaseRepository):
    async def get_galleries_list_by_user_id(self, user_id: int) -> GalleryListWithPositions:
        galleries_list = await queries.get_galleries_by_user_id(
            self.connection,
            user_id=user_id
        )
        if galleries_list:
            galleries = [PositionedGallery(**g)for g in galleries_list]
            return GalleryListWithPositions(galleries=galleries)
        else:
            return GalleryListWithPositions()

    async def create_gallery(self, name: str, user_id: int) -> GalleryWithId:
        async with self.connection.transaction():
            gallery_id, _ = await queries.create_new_gallery(
                self.connection,
                name=name
            )
            gallery_access = await queries.create_new_gallery_access(
                self.connection,
                gallery_id=gallery_id,
                user_id=user_id,
                is_owned=True
            )
        return GalleryWithId(gallery_id=gallery_id, name=name)

    async def rename_gallery(self, gallery_id: int, name: str, user_id: int) -> None:
        async with self.connection.transaction():
            await queries.update_gallery_name(
                self.connection,
                gallery_id=gallery_id,
                name=name
            )

    async def get_gallery_by_gallery_id(self, gallery_id: int, user_id: int) -> GalleryInDB:
        gallery = await queries.get_gallery_by_gallery_id(
            self.connection,
            gallery_id=gallery_id,
            user_id=user_id
        )
        if gallery:
            return GalleryInDB(
                gallery_id=gallery_id,
                name=gallery.get("name"),
                shared_uuid=gallery.get("shared_uuid"),
                created_at=gallery.get("created_at")
            )
        else:
            raise EntityDoesNotExist

    async def get_gallery_by_name(self, name: str, user_id: int) -> GalleryWithId:
        gallery = await queries.get_gallery_by_name(
            self.connection,
            name=name,
            user_id=user_id
        )
        if gallery:
            return GalleryWithId(gallery_id=gallery.get("id"), name=name)
        else:
            raise EntityDoesNotExist

    async def reorder_galleries(
            self,
            index: int,
            galleries_to_insert: List[int],
            galleries_list: GalleryListWithPositions
    ) -> None:
        if index == len(galleries_list.galleries):
            # if inserting at the end
            left_element = galleries_list.galleries[index - 1].position
            right_element = left_element + Decimal(len(galleries_to_insert) + 1)
        else:
            right_element = galleries_list.galleries[index].position

        if index:
            # if inserting at the start
            left_element = galleries_list.galleries[index - 1].position
        else:
            left_element = Decimal(0.0)

        positions = find_positions(
            left_element=left_element,
            right_element=right_element,
            insertion_index=index,
            new_elements_count=len(galleries_to_insert))
        bulk_data = [{
            "gallery_id": gallery_id,
            "position": position
        } for gallery_id, position in zip(galleries_to_insert, positions)]

        async with self.connection.transaction():
            await queries.change_galleries_position(self.connection, bulk_data)

    async def reorder_photos_in_gallery(
        self,
        gallery_id: int,
        index: int,
        photos_to_insert: List[str],
        photos_list: PhotoListWithPositions
    ) -> None:
        if index == len(photos_list.photos):
            # if inserting at the end
            left_element = photos_list.photos[index].position
            right_element = left_element + Decimal(len(photos_to_insert) + 1)
        else:
            right_element = photos_list.photos[index].position

        if index:
            # if inserting at the start
            left_element = photos_list.photos[index - 1].position
        else:
            left_element = Decimal(0.0)

        positions = find_positions(
            left_element=photos_list.photos[index -
                                            1].position if index else Decimal(0.0),
            right_element=photos_list.photos[index].position,
            insertion_index=index,
            new_elements_count=len(photos_to_insert))
        bulk_data = [{
            "photo_uuid": photo_uuid,
            "position": position,
            "gallery_id": gallery_id
        } for photo_uuid, position in zip(photos_to_insert, positions)]

        async with self.connection.transaction():
            await queries.change_photos_position(self.connection, bulk_data)

    async def remove_photos_from_gallery(
        self,
        gallery_id: int,
        photos: List[Photo]
    ) -> None:
        bulk_data = [{
            "photo_uuid": photo.photo_uuid,
            "gallery_id": gallery_id
        } for photo in photos]
        async with self.connection.transaction():
            await queries.remove_photos_from_gallery(
                self.connection,
                bulk_data
            )

    async def attach_photos_to_gallery(
        self,
        gallery_id: int,
        photos: List[Photo]
    ) -> None:
        # TODO check if photos belong to user
        bulk_data = [{
            "photo_uuid": photo.photo_uuid,
            "gallery_id": gallery_id
        } for photo in photos]
        async with self.connection.transaction():
            await queries.attach_photos_to_gallery(
                self.connection,
                bulk_data
            )

    async def delete_gallery(self, gallery_id: int) -> GalleryWithId:
        async with self.connection.transaction():
            await queries.delete_gallery(
                self.connection,
                gallery_id
            )
            # TODO update unattached photos

    async def get_shared_uuid(self, gallery: GalleryInDB) -> SharedLink:
        if not gallery.shared_uuid:
            # TODO generate UUID
            gallery.shared_uuid = "test"
            async with self.connection.transaction():
                await queries.update_shared_uuid(
                    self.connection,
                    shared_uuid=gallery.shared_uuid,
                    gallery_id=gallery.gallery_id
                )

        return SharedLink(shared_uuid=gallery.shared_uuid)

    async def remove_shared_uuid_by_gallery_id(self, gallery: GalleryInDB) -> None:
        shared_uuid = gallery.shared_uuid
        if shared_uuid:
            async with self.connection.transaction():
                await queries.remove_shared_uuid(
                    self.connection,
                    gallery_id=gallery.gallery_id
                )

    async def get_gallery_by_shared_uuid(self, shared_uuid: int) -> GalleryInDB:
        gallery = await queries.get_gallery_by_shared_uuid(
            self.connection,
            shared_uuid=shared_uuid
        )
        if not gallery:
            raise EntityDoesNotExist
        else:
            return GalleryInDB(**gallery)

    async def share_gallery(self, gallery: GalleryInDB, user_id: int) -> None:
        async with self.connection.transaction():
            await queries.create_new_gallery_access(
                self.connection,
                gallery_id=gallery.gallery_id,
                user_id=user_id,
                is_owned=False
            )
