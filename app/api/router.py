from app.api.routes import authentication, galleries, photos
from fastapi import APIRouter

router = APIRouter()
router.include_router(
    authentication.router,
    tags=["authentication"],
    prefix="/users"
)
router.include_router(
    galleries.router,
    tags=["galleries"],
    prefix="/galleries"
)
router.include_router(
    photos.router,
    tags=["photos"],
    prefix="/photos"
)
