from app.api.dependencies.auth_user import get_current_user_authorizer
from app.api.dependencies.repository import get_repository
from app.db.errors import EntityDoesNotExist
from app.db.repositories.galleries import GalleriesRepository
from app.db.repositories.photos import PhotosRepository
from app.models.galleries import (Gallery, GalleryInDB, GalleryInPatchRequest,
                                  GalleryListInReorderRequest,
                                  GalleryListInResponse, GalleryWithId,
                                  GalleryWithIdInResponse)
from app.models.photos import PhotoListInResponse
from app.models.shared_link import SharedLinkInResponse
from app.models.users import UserInDB
from app.resources import strings
from fastapi import APIRouter, Body, Depends, HTTPException, Response
from starlette.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

router = APIRouter()


@router.get(
    "",
    status_code=HTTP_200_OK,
    name="Get galleries as list of links",
    response_model=GalleryListInResponse
)
async def get_galleries(
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository))
) -> GalleryListInResponse:
    galleries_list = await galleries_repo.get_galleries_list_by_user_id(
        user_id=user.id
    )
    return galleries_list


@router.post(
    "",
    status_code=HTTP_200_OK,
    name="Create new gallery",
    response_model=GalleryWithIdInResponse
)
async def create_gallery(
    gallery_create: Gallery = Body(..., embed=True, alias="gallery"),
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository))
) -> GalleryWithIdInResponse:
    try:
        await galleries_repo.get_gallery_by_name(
            name=gallery_create.name,
            user_id=user.id
        )
    except EntityDoesNotExist:
        pass
    else:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.GALLERY_NAME_TAKEN,
        )

    gallery = await galleries_repo.create_gallery(
        name=gallery_create.name,
        user_id=user.id
    )
    return gallery


@router.patch(
    "",
    status_code=HTTP_200_OK,
    name="Reorder galleries in list of galleries",
    description="Index starts from 0"
)
async def reorder_galleries(
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository)),
    reoder_request: GalleryListInReorderRequest = Body(..., embed=True)
) -> Response:
    galleries_list = await galleries_repo.get_galleries_list_by_user_id(
        user_id=user.id
    )
    if reoder_request.new_index < 0 \
            or reoder_request.new_index > len(galleries_list.galleries):
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_REOEDER_INDEX,
        )
    if not galleries_list.galleries:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        )
    available_ids = set(g.gallery_id for g in galleries_list.galleries)
    if not all(g_id in available_ids for g_id in reoder_request.galleries):
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        )

    await galleries_repo.reorder_galleries(
        galleries_to_insert=reoder_request.galleries,
        index=reoder_request.new_index,
        galleries_list=galleries_list
    )
    return Response(status_code=HTTP_200_OK)


@router.get(
    "/{gallery_id}",
    status_code=HTTP_200_OK,
    name="Get all photos from the gallery as list of links",
    response_model=PhotoListInResponse
)
async def get_gallery_photos(
    gallery_id: int,
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository)),
    photos_repo: PhotosRepository = Depends(
        get_repository(PhotosRepository))
) -> PhotoListInResponse:
    try:
        await galleries_repo.get_gallery_by_gallery_id(
            gallery_id=gallery_id,
            user_id=user.id
        )
    except EntityDoesNotExist as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        ) from existence_error

    gallery_photos = await photos_repo.get_photos_by_gallery_id(
        gallery_id=gallery_id,
        user_id=user.id
    )
    return gallery_photos


@router.patch(
    "/{gallery_id}",
    status_code=HTTP_200_OK,
    name="Edit gallery: rename, attach photos, reorder photos, remove photos",
    description="Reorder index starts from 0"
)
async def edit_gallery(
    gallery_id: int,
    update_request: GalleryInPatchRequest = Body(
        ..., embed=True, alias="gallery"),
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository))
) -> Response:
    try:
        await galleries_repo.get_gallery_by_gallery_id(
            gallery_id=gallery_id,
            user_id=user.id
        )
    except EntityDoesNotExist as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        ) from existence_error

    if update_request.name:
        try:
            await galleries_repo.get_gallery_by_name(
                name=update_request.name,
                user_id=user.id
            )
        except EntityDoesNotExist:
            pass
        else:
            raise HTTPException(
                status_code=HTTP_400_BAD_REQUEST,
                detail=strings.GALLERY_NAME_TAKEN,
            )

        gallery = await galleries_repo.rename_gallery(
            gallery_id=gallery_id,
            name=update_request.name,
            user_id=user.id
        )
        return gallery

    if update_request.photos_to_attach:
        pass
        # TODO check photos and attach
    if update_request.photos_to_delete:
        pass
        # TODO check photos and delete
    if update_request.photos_to_reorder:
        pass
        # TODO check index range
        # TODO check photos and reorder
    return Response(status_code=HTTP_200_OK)


@router.delete(
    "/{gallery_id}",
    status_code=HTTP_200_OK,
    name="Delete gallery",
)
async def delete_gallery(
    gallery_id: int,
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository))
) -> Response:
    try:
        await galleries_repo.get_gallery_by_gallery_id(
            gallery_id=gallery_id,
            user_id=user.id
        )
    except EntityDoesNotExist as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        ) from existence_error

    await galleries_repo.delete_gallery(
        gallery_id=gallery_id
    )
    return Response(status_code=HTTP_200_OK)


@router.get(
    "/<{gallery_id}/shared_link",
    status_code=HTTP_200_OK,
    name="Get shared link for the gallery",
    response_model=SharedLinkInResponse
)
async def get_gallery_shared_link(
    gallery_id: int,
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository))
) -> SharedLinkInResponse:
    try:
        gallery = await galleries_repo.get_gallery_by_gallery_id(
            gallery_id=gallery_id,
            user_id=user.id
        )
    except EntityDoesNotExist as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        ) from existence_error

    gallery_shared_link = await galleries_repo.get_shared_uuid(
        gallery=gallery
    )
    return gallery_shared_link


@router.delete(
    "/<{gallery_id}/shared_link",
    status_code=HTTP_200_OK,
    name="Remove shared link for the gallery"
)
async def remove_shared_link_for_gallery(
    gallery_id: int,
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository))
) -> Response:
    try:
        gallery = await galleries_repo.get_gallery_by_gallery_id(
            gallery_id=gallery_id,
            user_id=user.id
        )
    except EntityDoesNotExist as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        ) from existence_error

    await galleries_repo.remove_shared_uuid_by_gallery_id(
        gallery=gallery
    )
    return Response(status_code=HTTP_200_OK)


@router.get(
    "/shared/{shared_uuid}",
    status_code=HTTP_200_OK,
    name="Join shared gallery and get a link to this gallery",
    response_model=GalleryWithIdInResponse
)
async def join_gallery(
    shared_uuid: str,
    user: UserInDB = Depends(get_current_user_authorizer()),
    galleries_repo: GalleriesRepository = Depends(
        get_repository(GalleriesRepository))
) -> GalleryWithIdInResponse:
    try:
        gallery = await galleries_repo.get_gallery_by_shared_uuid(
            shared_uuid=shared_uuid
        )
    except EntityDoesNotExist as existence_error:
        raise HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail=strings.INCORRECT_GALLERY_ID,
        ) from existence_error

    await galleries_repo.share_gallery(
        gallery=gallery,
        user_id=user.id
    )
    return gallery
