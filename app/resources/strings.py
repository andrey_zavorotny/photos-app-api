# API messages

USER_DOES_NOT_EXIST_ERROR = "user does not exist"

INCORRECT_LOGIN_INPUT = "incorrect email or password"
EMAIL_TAKEN = "user with this email already exists"

WRONG_TOKEN_PREFIX = "unsupported authorization type"  # noqa: S105
MALFORMED_PAYLOAD = "could not validate credentials"

AUTHENTICATION_REQUIRED = "authentication required"

GALLERY_NAME_TAKEN = "gallery name taken"
INCORRECT_GALLERY_ID = "gallery does not exist or forbidden"
INCORRECT_SHARED_LINK = "shared link is broken or removed by the owner"
INCORRECT_PHOTO_UUID = "gallery does not exist or forbidden"
INCORRECT_REOEDER_INDEX = "the index must be from 0 to the last index of the list - 1"